package co.com.choucair.certification.proyectotest.tasks;

import co.com.choucair.certification.proyectotest.userinterface.RegisterPageThree;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class RegisterInPageThree implements Task {


    public static RegisterInPageThree register() {
        return Tasks.instrumented(RegisterInPageThree.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(2000);
            actor.attemptsTo(
                    Click.on(RegisterPageThree.BUTTON_STEP)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
