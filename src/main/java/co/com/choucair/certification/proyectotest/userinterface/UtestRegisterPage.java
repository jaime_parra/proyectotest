package co.com.choucair.certification.proyectotest.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class UtestRegisterPage {
    public static final Target REGISTER_BUTTON = Target.the("button for go register page").located(By.xpath("//a[@class='unauthenticated-nav-bar__sign-up']"));
}
