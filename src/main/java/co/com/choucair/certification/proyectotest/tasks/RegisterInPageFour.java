package co.com.choucair.certification.proyectotest.tasks;

import co.com.choucair.certification.proyectotest.userinterface.RegisterPageFour;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegisterInPageFour implements Task {
    private String strPass;


    public RegisterInPageFour(String strPass) {
        this.strPass = strPass;

    }

    public static RegisterInPageFour register(String strPass) {
        return Tasks.instrumented(RegisterInPageFour.class, strPass);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(strPass).into(RegisterPageFour.INPUT_PASS),
                Enter.theValue(strPass).into(RegisterPageFour.INPUT_CONFIRM),
                Click.on(RegisterPageFour.CHECKBOX_TERMS),
                Click.on(RegisterPageFour.CHECKBOX_PRIVACY),
                Click.on(RegisterPageFour.BUTTON_REGISTER)
        );
    }
}
