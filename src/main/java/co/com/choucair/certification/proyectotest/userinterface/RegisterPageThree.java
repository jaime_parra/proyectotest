package co.com.choucair.certification.proyectotest.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class RegisterPageThree  {
    public static final Target BUTTON_STEP = Target.the("button next").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
}
