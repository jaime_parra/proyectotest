package co.com.choucair.certification.proyectotest.question;

import co.com.choucair.certification.proyectotest.userinterface.RegisterPageFour;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String question;
    public Answer(String question){
        this.question = question;
    }

    public static Answer toThe(String question){
        return new Answer(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean result;
        String welcome = Text.of(RegisterPageFour.WELCOME).viewedBy(actor).asString();
        if(question.equals(welcome))
            result = true;
        else
            result = false;
        return result;
    }
}
