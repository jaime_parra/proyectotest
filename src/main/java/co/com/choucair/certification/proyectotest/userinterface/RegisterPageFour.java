package co.com.choucair.certification.proyectotest.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class RegisterPageFour  {
    public static final Target INPUT_PASS = Target.the("input pass").located(By.id("password"));
    public static final Target INPUT_CONFIRM = Target.the("input confirm").located(By.id("confirmPassword"));
    public static final Target CHECKBOX_TERMS = Target.the("checkbox terms and condition").located(By.id("termOfUse"));
    public static final Target CHECKBOX_PRIVACY = Target.the("checkbox policy the privacity").located(By.id("privacySetting"));
    public static final Target BUTTON_REGISTER = Target.the("button register").located(By.xpath("//a[@class='btn btn-blue']"));
    public static final Target WELCOME = Target.the("extract text welcome").located(By.xpath("//h1[contains(text(),'Welcome')]"));

}
