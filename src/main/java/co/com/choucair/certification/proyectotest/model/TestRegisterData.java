package co.com.choucair.certification.proyectotest.model;

public class TestRegisterData {
    private String strName;
    private String strLastName;
    private String strEmail;
    private String strPass;
    private String strWelcome;

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrPass() {
        return strPass;
    }

    public void setStrPass(String strPass) {
        this.strPass = strPass;
    }

    public String getStrWelcome() {
        return strWelcome;
    }

    public void setStrWelcome(String strWelcome) {
        this.strWelcome = strWelcome;
    }
}
