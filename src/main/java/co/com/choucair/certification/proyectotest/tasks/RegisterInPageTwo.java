package co.com.choucair.certification.proyectotest.tasks;

import co.com.choucair.certification.proyectotest.userinterface.RegisterPageTwo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class RegisterInPageTwo implements Task {

    public static RegisterInPageTwo register() {
        return Tasks.instrumented(RegisterInPageTwo.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(2000);
            actor.attemptsTo(
                    Click.on(RegisterPageTwo.BUTTON_STEP)
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
