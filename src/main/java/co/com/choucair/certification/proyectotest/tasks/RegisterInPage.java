package co.com.choucair.certification.proyectotest.tasks;

import co.com.choucair.certification.proyectotest.userinterface.RegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class RegisterInPage implements Task {
    private String strName;
    private String strLastName;
    private String strEmail;

    public RegisterInPage(String strName, String strLastName, String strEmail) {
        this.strName = strName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
    }

    public static RegisterInPage register(String strName, String strLastName, String strEmail) {
        return Tasks.instrumented(RegisterInPage.class, strName, strLastName,strEmail);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(strName).into(RegisterPage.INPUT_NAME),
                Enter.theValue(strLastName).into(RegisterPage.INPUT_LAST),
                Enter.theValue(strEmail).into(RegisterPage.INPUT_EMAIL),
                Click.on(RegisterPage.SELECT_MONTH),
                Click.on(RegisterPage.SELECT_DAY),
                Click.on(RegisterPage.SELECT_YEAR),
                Click.on(RegisterPage.BUTTON_ONE)
        );
    }
}
