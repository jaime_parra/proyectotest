package co.com.choucair.certification.proyectotest.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class RegisterPage  {

    public static final Target INPUT_NAME = Target.the("input name").located(By.id("firstName"));
    public static final Target INPUT_LAST = Target.the("input last name").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("input email").located(By.id("email"));
    public static final Target SELECT_MONTH = Target.the("select month").located(By.xpath("//select[@id='birthMonth']//option[contains(text(),'April')]"));
    public static final Target SELECT_DAY = Target.the("select day").located(By.xpath("//select[@id='birthDay']//option[contains(text(),'29')]"));
    public static final Target SELECT_YEAR = Target.the("select year").located(By.xpath("//select[@id='birthYear']//option[contains(text(),'1995')]"));
    public static final Target BUTTON_ONE = Target.the("button next 1").located(By.xpath("//a[@class='btn btn-blue']"));
}
