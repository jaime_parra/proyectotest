package co.com.choucair.certification.proyectotest.stepdefinitions;

import co.com.choucair.certification.proyectotest.model.TestRegisterData;
import co.com.choucair.certification.proyectotest.question.Answer;
import co.com.choucair.certification.proyectotest.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class TestRegisterStepDefinitions {
    @Before
    public void setStage(){ OnStage.setTheStage(new OnlineCast());}

    @Given("^than joshua enter the utest register page$")
    public void thanJoshuaEnterTheUtestRegisterPage() {
        OnStage.theActorCalled("Joshua").wasAbleTo(OpenUp.thePage(), GoToRegister.selectRegister());
    }


    @When("^he enters the registration data step one$")
    public void heEntersTheRegistrationDataStepOne(List<TestRegisterData> TestRegisterData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(RegisterInPage.register(TestRegisterData.get(0).getStrName(),
                TestRegisterData.get(0).getStrLastName(),TestRegisterData.get(0).getStrEmail()));
    }

    @And("^he enters the registration data step two$")
    public void heEntersTheRegistrationDataStepTwo(){
        OnStage.theActorInTheSpotlight().attemptsTo(RegisterInPageTwo.register());
    }

    @And("^he enters the registration data step three$")
    public void heEntersTheRegistrationDataStepThree() {
        OnStage.theActorInTheSpotlight().attemptsTo(RegisterInPageThree.register());
    }

    @And("^he enters the registration data step four$")
    public void heEntersTheRegistrationDataStepFour(List<TestRegisterData> TestRegisterData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(RegisterInPageFour.register(TestRegisterData.get(0).getStrPass()));
    }

    @Then("^he registered on the page$")
    public void heRegisteredOnThePage(List<TestRegisterData> TestRegisterData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(TestRegisterData.get(0).getStrWelcome())));
    }
}
