#Autor Jaime Parra Cardona - parracardonajaime@gmail.com - +57 3232219778
@stories
Feature: Test Register
  As a user, I want to register on the test page
  @scenario1
  Scenario: register on the page
    Given than joshua enter the utest register page
    When he enters the registration data step one
      |strName     |strLastName       |strEmail                     |
      |Juan       |Pedro             |juanajose1712123@gmail.com           |
    And he enters the registration data step two

    And he enters the registration data step three

    And he enters the registration data step four
      | strPass    |
      |1_Registro12 |
    Then he registered on the page
    |strWelcome                                                                |
    |Welcome  |

